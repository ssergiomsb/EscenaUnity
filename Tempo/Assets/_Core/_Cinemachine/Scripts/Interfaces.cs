﻿using UnityEngine;

namespace VirtualCameraController
{
    public interface IVirtualCameraTarget
    {
        void OnLookAtChange(GameObject lookAtTarget);
        void OnFollowChange(GameObject followTarget);
    }
}