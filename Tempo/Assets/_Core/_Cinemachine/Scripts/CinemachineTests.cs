﻿using UnityEngine;

namespace VirtualCameraController
{
    public class CinemachineTests : MonoBehaviour
    {
        [SerializeField]
        private Transform _lookAtTarget = default;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
                VirtualCamerasController.Instance.LookTarget = _lookAtTarget;
        }
    }
}