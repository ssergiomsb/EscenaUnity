﻿using UnityEngine;

namespace VirtualCameraController
{
    /// <summary>
    /// Delimita una zona de la escena y controla cuando se ha entrado en ella, para activar una cámara virtual
    /// </summary>
    public class CameraZone : MonoBehaviour
    {
        [SerializeField]
        private VirtualCameraNode _virtualCameraNode = default;
        
        public void OnTriggerEnter(Collider other)
        {
            VirtualCamerasController.Instance.CurrentPlayerCameraNode = _virtualCameraNode;
        }
    }
}