﻿using UnityEngine;
using Cinemachine;

namespace VirtualCameraController
{
    /// <summary>
    /// Nodo usado para manejar una cámara virtual con el "Aim" configurado en el modo "Group Composer". Este modo tiene un grupo de targets, que encuadrar dentro del campo de visión de la cámara en base a prioridades.
    /// </summary>
    public class Compound_VC_Node : VirtualCameraNode
    {
        [Header("Target group")]
        [SerializeField]
        CinemachineTargetGroup _cinemachineTargetGroup = default;

        /// <summary>
        /// Lista actual con todos los targets
        /// </summary>
        Cinemachine.CinemachineTargetGroup.Target[] _currentTargets;

        /// <summary>
        /// Target principal. Normalmente suele ser el jugador
        /// </summary>
        [SerializeField]
        Cinemachine.CinemachineTargetGroup.Target _principalTarget = default;

        /// <summary>
        /// Targets secudarios que deben encuadrarse dentro de la visión de la cámara
        /// </summary>
        [SerializeField]
        Cinemachine.CinemachineTargetGroup.Target[] _secondaryTargets = default;

        /// <summary>
        /// Devuelve o actualiza el target principal
        /// </summary>
        public override Transform VirtualCamera_LookAt
        {
            get { return _currentTargets[0].target; }
            set 
            {
                //Actualiza el target principal en la lista
                _currentTargets[0].target = value;

                //Actualiza la lista de targets en el TargetGroup
                _cinemachineTargetGroup.m_Targets = _currentTargets;
            }
        }

        protected override void Start()
        {
            base.Start();

            //Crea un array en el que guardar el target principal junto a los secudarios
            _currentTargets = new CinemachineTargetGroup.Target[_secondaryTargets.Length+1];

            //Añade el target principal en la posición 0
            _currentTargets[0] = _principalTarget;

            //El resto de targets los añade consecutivamente
            for (int i=0; i < _secondaryTargets.Length; i++)
                _currentTargets[i+1] = _secondaryTargets[i];

            //Actualiza la lista de targets en el TargetGroup
            _cinemachineTargetGroup.m_Targets = _currentTargets;
        }

    }
}