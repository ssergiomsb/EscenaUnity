﻿using UnityEngine;

namespace VirtualCameraController
{
    /// <summary>
    /// Constantes para asignar las prioridades de las cámaras virtuales
    /// </summary>
    public static class Priority
    {
        //Prioridad para cámaras que no se están usando
        public const int LOW = 1;

        //Prioridad de la cámara del jugador
        public const int PLAYER = 50;

        //Prioridad máxima para usar una cámara por encima de todas las demás
        public const int HIGHT = 100;
    }
}