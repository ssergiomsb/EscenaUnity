﻿using Cinemachine;
using UnityEngine;

namespace VirtualCameraController
{
    /// <summary>
    /// Controla las distintas cámaras virtuales de la escena (Prioridades, follow target, look target, etc)
    /// </summary>
    public class VirtualCamerasController : MonoBehaviour
    {
        private static VirtualCamerasController _instance;

        public static VirtualCamerasController Instance { get { return _instance; } }

        /// <summary>
        /// Evento para actualizar el objetivo al que están mirando las cámaras virtuales
        /// </summary>
        public delegate void OnLookAtChangeDelegate(Transform lookAtTarget);
        public event OnLookAtChangeDelegate OnLookAtChange;

        /// <summary>
        /// Evento para actualizar el objetivo al que están siguiendo las cámaras virtuales
        /// </summary>
        public delegate void OnFollowChangeDelegate(Transform followTarget);
        public event OnFollowChangeDelegate OnFollowChange;

        /// <summary>
        /// Cámara inicial al cargar la escena
        /// </summary>
        [SerializeField]
        private VirtualCameraNode _initVirtualCameraNode = default;

        /// <summary>
        /// Cámara que está usando actualmente el player
        /// </summary>
        private VirtualCameraNode _currentVirtualCameraNode;

        public Transform LookTarget
        {
            set { OnLookAtChange(value); }
        }

        public Transform FollowTarget
        {
            set { OnFollowChange(value); }
        }

        public VirtualCameraNode CurrentPlayerCameraNode
        {
            set
            {
                //Si había anterioremente una cámara
                if (_currentVirtualCameraNode != null)
                {
                    //Disminuye su prioridad
                    _currentVirtualCameraNode.CurrentVirtualCamera.Priority = Priority.LOW;
                    //Desactiva el VirtualCameraNode
                    _currentVirtualCameraNode.IsActive = false;
                }


                //Establece el nuevo nodo
                _currentVirtualCameraNode = value;
                //Aumenta la prioridad de la cámara
                _currentVirtualCameraNode.CurrentVirtualCamera.Priority = Priority.PLAYER;
                //Activa el VirtualCameraNode
                _currentVirtualCameraNode.IsActive = true;
            }
        }

        void Awake()
        {
            _instance = this;
        }

        private void Start()
        {
            CurrentPlayerCameraNode = _initVirtualCameraNode;
        }
    }
}