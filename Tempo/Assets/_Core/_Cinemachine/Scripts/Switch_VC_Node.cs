﻿using Cinemachine;
using UnityEngine;

namespace VirtualCameraController
{
    public class Switch_VC_Node : VirtualCameraNode
    {
        [SerializeField]
        protected CinemachineVirtualCameraBase _mainVirtualCamera = default;
        [SerializeField]
        protected CinemachineVirtualCameraBase _secondaryVirtualCamera = default;

        bool _isMainCameraActive;

        protected override void Start()
        {
            base.Start();

            _isMainCameraActive = true;
            CurrentVirtualCamera = _mainVirtualCamera;
        }

        private void Update()
        {
            if (!IsActive)
                return;

            if (Input.GetKeyDown(KeyCode.C))
                SwitchCurrentCamera();
        }

        /// <summary>
        /// Cambia la cámara actualmente activa en el nodo
        /// </summary>
        void SwitchCurrentCamera()
        {
            _isMainCameraActive = !_isMainCameraActive;

            //Disminuye la prioridad de la cámara actual
            CurrentVirtualCamera.Priority = Priority.LOW;

            //Cambia la cámara actual
            if (_isMainCameraActive)
                CurrentVirtualCamera = _mainVirtualCamera;
            else
                CurrentVirtualCamera = _secondaryVirtualCamera;

            //Aumenta la prioridad de la cámara actual
            CurrentVirtualCamera.Priority = Priority.PLAYER;
        }
    }
}