﻿/*
* Copyright (c) Gunstar Studio
*/
using SO;
using UnityEngine;

/// <summary>
/// Sincroniza la posición de un objeto con respecto a otro en el eje Y
/// </summary>
public class SyncYPosition : MonoBehaviour
{
    private Vector3 _position;
    [SerializeField]
    private TransformVariable _targetTransformVariable = default;

    private void Start()
    {
        _position = this.transform.position;
    }

    private void Update()
    {
        _position.y = _targetTransformVariable.Value.position.y;
        this.transform.position = _position;
    }
}
