﻿using Cinemachine;
using UnityEngine;

namespace VirtualCameraController
{
    public class VirtualCameraNode : MonoBehaviour
    {
        bool _isActive;

        [SerializeField]
        protected CinemachineVirtualCameraBase _currentVirtualCamera;

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public CinemachineVirtualCameraBase CurrentVirtualCamera
        {
            get { return _currentVirtualCamera; }
            set { _currentVirtualCamera = value; }
        }

        public virtual Transform VirtualCamera_LookAt
        {
            get { return _currentVirtualCamera.LookAt; }
            set { _currentVirtualCamera.LookAt = value; }
        }

        public virtual Transform VirtualCamera_Follow
        {
            get { return _currentVirtualCamera.Follow; }
            set { _currentVirtualCamera.Follow = value; }
        }

        /// <summary>
        /// Indica si debe actualizar el target al que apuntar cada vez que se actualice en el VirtualCamerasController
        /// </summary>
        [SerializeField]
        protected bool _updateLoolAt = false;
        /// <summary>
        /// Indica si debe actualizar el target al que seguir cada vez que se actualice en el VirtualCamerasController
        /// </summary>
        [SerializeField]
        protected bool _updateFollow = false;

        protected virtual void Start()
        {
            _isActive = false;

            if (_updateLoolAt)
                VirtualCamerasController.Instance.OnLookAtChange += OnLookAtChanged;

            if (_updateFollow)
                VirtualCamerasController.Instance.OnFollowChange += OnFollowChange;
        }

        void OnDestroy()
        {
            if (_updateLoolAt)
                VirtualCamerasController.Instance.OnLookAtChange -= OnLookAtChanged;

            if (_updateFollow)
                VirtualCamerasController.Instance.OnFollowChange -= OnFollowChange;
        }

        void OnLookAtChanged(Transform lookAtTarget)
        {
            VirtualCamera_LookAt = lookAtTarget;
        }

        void OnFollowChange(Transform followTarget)
        {

            VirtualCamera_Follow = followTarget;
        }
    }
}