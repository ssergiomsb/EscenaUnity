﻿using UnityEngine;

namespace SA
{
	/// <summary>
	/// Contiene los datos necesarios para saltar obstáculos
	/// </summary>
	[System.Serializable]
	public class VaultData
	{
		/// <summary>
		/// Posición donde comienza el salto del obstáculo
		/// </summary>
		public Vector3 startPosition;
		/// <summary>
		/// Posición donde termina el salto
		/// </summary>
		public Vector3 endingPosition;
		/// <summary>
		/// Velocidad a la que se realiza la transición entre la posición de inicio y la final
		/// </summary>
		public float vaultSpeed =  2;

		/// <summary>
		/// Valor de 0 a 1 que indica en que momento del desplazamiento de encuentra
		/// </summary>
		public float vaultT;
		/// <summary>
		/// Indica si se ha iniciado el desplazamiento
		/// </summary>
		public bool isInit;
	}
}