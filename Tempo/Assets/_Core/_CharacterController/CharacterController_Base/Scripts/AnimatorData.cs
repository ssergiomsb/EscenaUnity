﻿using UnityEngine;

namespace SA
{
	/// <summary>
	/// Contiene algunos datos del animator como los transform de ciertos huesos
	/// </summary>
	public class AnimatorData
	{
		public Transform leftFoot;
		public Transform rightFoot;

		public AnimatorData(Animator anim)
		{
			leftFoot = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
			rightFoot = anim.GetBoneTransform(HumanBodyBones.RightFoot);
		}
	}
}