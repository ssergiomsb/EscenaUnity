﻿
namespace SA
{
	/// <summary>
	/// Se usa como transición entre un estado y otro en base a una condición
	/// </summary>
    [System.Serializable]
    public class Transition
    {
        public int id;
        public Condition condition;
        public State targetState;
        public bool disable;
    }
}