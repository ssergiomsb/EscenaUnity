﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Comprueba si se cumplen las condiciones necesarias para realizar un salto
    /// </summary>
    [CreateAssetMenu(menuName = "Conditions/Jump Condition")]
    public class JumpCondition : Condition
    {
        ///<summary>
        ///Tiempo que puede transcurrir, después de haber dejado de estar sobre el suelo, durante el cual te seguirá permitiendo saltar
        ///</summary>
        ///<remarks>
        ///Cuando se considera que se ha dejado de estar sobre el suelo:
        ///- Si está activado AllowJumpingWhenSliding: Se considera que ha dejado el suelo si el character está en el aire
        ///- Si está desactivado AllowJumpingWhenSliding: Se considera que ha dejado el suelo si está en el aire, o está deslisándose sobre este (Suelo inestable)
        ///</remarks>
        [SerializeField]
        private float _jumpPostGroundingGraceTime = 0.0f;

        public override bool CheckCondition(StateManager states)
        {
            bool resp = false;

            if (states._jumpVariables.JumpRequested)
            {
                //Si no se ha superado el tiempo de gracia, para poder saltar después de haber dejado de estar sobre un suelo sobre el que si se podía; O estamos actualmente en un state que nos permita saltar
                if (states._jumpVariables.TimeSinceLastAbleToJump <= _jumpPostGroundingGraceTime || states.CurrentState.name.CompareTo(StateConstant.LEDGE_CLIMB) == 0)
                {
                    resp = true;
                    states._jumpVariables.IsJumping = true;
                } else
                    states._jumpVariables.JumpRequested = false;
            }

            //Si está sobre un suelo sobre el que se permita saltar
            if ((states._jumpVariables.AllowJumpingWhenSliding ? states._motor.GroundingStatus.FoundAnyGround : states._motor.GroundingStatus.IsStableOnGround))
                states._jumpVariables.TimeSinceLastAbleToJump = 0.0f;
            else
                states._jumpVariables.TimeSinceLastAbleToJump += states.DeltaTimeMotor; //Nota: Realizar las comprobaciones (Como si se cumple el tiempo de gracia), siempre antes de incrementar este tiempo

            return resp;
        }
    }
}