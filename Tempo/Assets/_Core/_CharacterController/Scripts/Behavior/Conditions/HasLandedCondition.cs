﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Comprueba si ha aterrizado sobre suelo estable
    /// </summary>
    [CreateAssetMenu(menuName = "Conditions/KHasLanded")]
    public class HasLandedCondition : Condition
    {
        private bool result;

        public override bool CheckCondition(StateManager states)
        {
            bool resp = false;

            // Descomentar la siguiente linea si queremos que se pueda considerar como que ha aterrizado sobre el suelo cuando se esté deslizando
            // if ((states._jumpVariables.AllowJumpingWhenSliding ? states._motor.GroundingStatus.FoundAnyGround : states._motor.GroundingStatus.IsStableOnGround))
            if (states._motor.GroundingStatus.IsStableOnGround)
            {
                //Si no ha saltado en este frame. Se da el salto por finalizado
                if (!states._jumpVariables.JumpedThisFrame)
                    resp = true;
            }

            return resp;
        }
    }
}