﻿using UnityEngine;
using SO;

namespace SA
{
    /// <summary>
    /// Rota un objeto en base a la posición de la cámara
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Rotation/Kinematic Rotate Based On Camera Orientation")]
    public class KRotateBasedOnCamera : StateActions
    {
        [SerializeField]
        private TransformVariable _cameraTransform = default;
        /// <summary>
        /// Suavidad con la que se rota
        /// </summary>
        [SerializeField]
        private float _orientationSharpness = 10.0f;

        public override void Execute(StateManager states)
        {
            //Si la cámara es nula
            if (_cameraTransform.value == null)
                return;

            //Recibe los valores del eje horizontal y vertical del input manager
            float h = states._movementVariables.Horizontal;
            float v = states._movementVariables.Vertical;

            //Calcula la dirección en base a los dos ejes anteriores
            Vector3 targetDir = _cameraTransform.value.forward * v;
            targetDir += _cameraTransform.value.right * h;
            targetDir.Normalize();

            //Se establece el eje Y a cero, para que el character no rote sobre este
            targetDir.y = 0.0f;

            //Si la dirección es igual a cero, establece el vector forward del character como dirección
            if (targetDir == Vector3.zero)
                targetDir = states.CharacterTransform.forward;

            //Actualiza la dirección objetivo actual, para que pueda ser usada por otros StateActions (Ej: KMovement)
            states._movementVariables.TargetDirection = targetDir;

            //Interpola suavemente entre la dirección actual y la dirección hacia la que debe mirar
            Vector3 smoothedLookInputDirection = Vector3.Slerp(states._motor.CharacterForward, targetDir, 1 - Mathf.Exp(-_orientationSharpness * states.DeltaTimeMotor)).normalized;

            //Establece la rotación del character
            states._currentRotationMotor = Quaternion.LookRotation(smoothedLookInputDirection, states._motor.CharacterUp);
        }
    }
}