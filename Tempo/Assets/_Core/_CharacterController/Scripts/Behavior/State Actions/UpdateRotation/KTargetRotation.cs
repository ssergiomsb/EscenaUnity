﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Rota el character hacia una dirección que se ha establecido previamente (Ej: LedgeClimbCondition)
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Rotation/Target Rotation Based On A Direcction")]
    public class KTargetRotation : StateActions
    {
        [SerializeField]
        private float _orientationSharpness = 10f;
        private Vector3 _smoothedLookDirection;

        public override void Execute(StateManager states)
        {
            //Interpola suavemente entre la dirección actual y la dirección hacia la que debe mirar
            _smoothedLookDirection = Vector3.Slerp(states._motor.CharacterForward, states._movementVariables.TargetDirection, 1 - Mathf.Exp(-_orientationSharpness * states.DeltaTimeMotor)).normalized;

            //Establece la rotación del character
            states._currentRotationMotor = Quaternion.LookRotation(_smoothedLookDirection, states._motor.CharacterUp);
        }
    }
}