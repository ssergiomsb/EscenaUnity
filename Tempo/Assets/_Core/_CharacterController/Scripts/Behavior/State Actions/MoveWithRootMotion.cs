﻿using UnityEngine;

namespace SA
{
	/// <summary>
	/// Mueve el personaje en base a la velocidad/deltaPosition de la animación que se está ejecutando
	/// </summary>
	[CreateAssetMenu(menuName ="Actions/State Actions/Move With Root Motion")]
	public class MoveWithRootMotion : StateActions
	{
		public override void Execute(StateManager states)
		{
			//Es necesario resetearlo a 0 en cada frame para que el gameobject del animator no se descentre del padre
			states.CharacterAnimator.transform.localPosition = Vector3.zero;
			states.GetComponent<Rigidbody>().drag = 0;
			Vector3 v = states.GetComponent<Rigidbody>().velocity;
			Vector3 targetV = states.CharacterAnimator.deltaPosition;
			//En este caso se multiplica la velocidad por 60 para exagerar mas el movimiento producido por la animación
			targetV *= 60;
			targetV.y = v.y;
			states.GetComponent<Rigidbody>().velocity = targetV;
		}
	}
}