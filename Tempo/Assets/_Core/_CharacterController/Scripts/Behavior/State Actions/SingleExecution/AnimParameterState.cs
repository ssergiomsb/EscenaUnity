﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Modificar el valor de un parámetro del animator
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Single Execution/Animator Parameter State")]
    public class AnimParameterState : StateActions
    {
        [SerializeField]
        private bool _value = true;
        [SerializeField]
        private string _hash = default;

        /*TERMINAR - Permitir que puedas configurar que tipo de parámetro que modificar*/
        public override void Execute(StateManager states)
        {
            states.CharacterAnimator.SetBool(Animator.StringToHash(_hash), _value);
        }
    }
}