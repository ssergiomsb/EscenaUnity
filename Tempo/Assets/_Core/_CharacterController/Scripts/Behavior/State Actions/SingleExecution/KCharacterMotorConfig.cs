﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Establece unos parámetros de configuración para el kinematic character motor
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Single Execution/Kinematic Character Motor Config")]
    public class KCharacterMotorConfig : StateActions
    {
        [SerializeField]
        private bool _movementCollisionSolving = true;
        [SerializeField]
        private bool _groundSolving = true;
        [SerializeField]
        private bool _capsuleCollision = true;

        public override void Execute(StateManager states)
        {
            states._motor.SetMovementCollisionsSolvingActivation(_movementCollisionSolving);
            states._motor.SetGroundSolvingActivation(_groundSolving);
            states._motor.SetCapsuleCollisionsActivation(_capsuleCollision);
        }
    }
}