﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Restablece los valores por defecto de las variables del salto
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Single Execution/Restore jump")]
    public class RestoreJump : StateActions
    {
        public override void Execute(StateManager states)
        {
            states._jumpVariables.RestoreJump();
        }
    }
}