﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Realiza un crossfade a un nuevo state del animator del character
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Single Execution/Animator Cross Fade")]
    public class AnimCrossFade : StateActions
    {
        [SerializeField]
        private float _normalizedTransitionDUration = 0.0f;
        [SerializeField]
        private string _hash = default;

        public override void Execute(StateManager states)
        {
            states.CharacterAnimator.CrossFade(_hash, _normalizedTransitionDUration);
        }
    }
}