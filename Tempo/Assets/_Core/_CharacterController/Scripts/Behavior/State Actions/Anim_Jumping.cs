﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Controla la animación de salto del personaje
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Animations/Anim_Jumping")]
    public class Anim_Jumping : StateActions
    {

        public override void Execute(StateManager states)
        {
            /*Los valores de la capsula deben modificarse cuando se está haciendo la transición a la animación del salto, mientras esta se reproduce completa, pero no cuando en la transición de salida a la siguiente, si no creará una interpolación de estos valores con los de la siguiente*/
            //Si la siguiente animación a la que se está haciendo la transición es la de salto, o la animación actual es la de salto y no se está realizando ninguna transición
            if (states.CharacterAnimator.GetNextAnimatorStateInfo(1).IsName(AnimConstant.ANIM_JUMP_FORWARD) 
                || (states.CharacterAnimator.GetCurrentAnimatorStateInfo(1).IsName(AnimConstant.ANIM_JUMP_FORWARD) && !states.CharacterAnimator.IsInTransition(states.CharacterAnimator.GetLayerIndex(AnimConstant.LAYER_OVERRIDE_ALL))))
            {
                states._motor.SetCapsuleDimensions(CharacterMotorConstant.CAPSULE_RADIUS, CharacterMotorConstant.CAPSULE_COLLIDER_HEIGHT, states.CharacterAnimator.GetFloat(AnimConstant.PARAM_COLLIDER_Y));
                //states._motor.SetCapsuleDimensions(0.7f, states.anim.GetFloat(AnimConstant.PARAM_COLLIDER_HEIGHT), states.anim.GetFloat(AnimConstant.PARAM_COLLIDER_Y));
            }
        }
    }
}