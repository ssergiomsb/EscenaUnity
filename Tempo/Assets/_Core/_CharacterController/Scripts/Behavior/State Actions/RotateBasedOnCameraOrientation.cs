﻿using UnityEngine;
using SO;

namespace SA
{
	/// <summary>
	/// Rota un objeto en base a la posición de la cámara
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/State Actions/Rotate Based On Camera Orientation")]
	public class RotateBasedOnCameraOrientation : StateActions
	{
		
		public TransformVariable cameraTransform;

		private float speed = 8;

		public override void Execute(StateManager states)
		{
			//Si la cámara es nula
			if (cameraTransform.value == null)
				return;

			//Recibe los valores del eje horizontal y vertical del input manager
			float h = states._movementVariables.Horizontal;
			float v = states._movementVariables.Vertical;

			Vector3 targetDir = cameraTransform.value.forward * v;
			targetDir += cameraTransform.value.right * h;
			targetDir.Normalize();

			targetDir.y = 0.0f;
			//Si la dirección es igual a cero, rota hacia el frente con respecto de la cámara
			if (targetDir == Vector3.zero)
				targetDir = states.CharacterTransform.forward;

			Quaternion tr = Quaternion.LookRotation(targetDir);
            //Interpola entre la rotación actual y la objetivo, en base a una velocidad de rotación y cuánto se esté movimiento ese objeto (A mas movimiento mas rotación)
            Quaternion targetRotation = Quaternion.Slerp(
				states.CharacterTransform.rotation, tr,
				states.DeltaTimeMono * states._movementVariables.MovementeAmount * speed);

            //Aplica la rotación interpolada al objeto
            states.CharacterTransform.rotation = targetRotation;

		}
	}
}