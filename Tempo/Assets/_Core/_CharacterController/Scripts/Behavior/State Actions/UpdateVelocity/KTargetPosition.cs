﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Actualiza la velocidad del character para que se coloque a una posición determinada
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Velocity/Target Position Movement")]
    public class KTargetPosition : StateActions
    {
        public override void Execute(StateManager states)
        {
            //Calcula y establece la velocidad que es necesaria para que se mueva a la posición objetivo
            states._currentVelocityMotor = states._motor.GetVelocityForMovePosition(states._motor.TransientPosition, states._movementVariables.TargetPosition, states.DeltaTimeMotor);
        }
    }
}