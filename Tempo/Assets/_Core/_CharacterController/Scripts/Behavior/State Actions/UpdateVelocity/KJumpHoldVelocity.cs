﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Suma una velocidad extra durante un tiempo establecido al salto, mientras se mantenga pulsado el botón del salto. 
    /// </summary>
    /// <remarks>
    /// Se utiliza para que el character salte mas o menos distancia en base al tiempo que mantengamos pulsado el botón
    /// </remarks>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Velocity/Kinematic Jump Hold Update Velocity")]
    public class KJumpHoldVelocity : StateActions
    {
        //Velocidad extra por segundo que suma
        public float _jumpSpeed = 24.0f;
        //Tiempo máximo durante el cual aplica la velocidad extra
        public float _maxTimeJumpHold = 0.3f;

        public override void Execute(StateManager states)
        {
            if (states._jumpVariables.JumpCurrentVariable && states._jumpVariables.CurrentTimeJumping <= _maxTimeJumpHold && !states._jumpVariables.JumpHoldBreakUp)
            {
                states._jumpVariables.CurrentTimeJumping += states.DeltaTimeMotor;

                // Se usa el vector up para establecer la dirección en la que se suma el impulso extra al salto
                Vector3 jumpDirection = states._motor.CharacterUp;

                states._motor.ForceUnground();

                // Incrementa la velocidad de salto
                states._currentVelocityMotor += (jumpDirection * _jumpSpeed) * states.DeltaTimeMotor;
            }

            //Si se deja de pulsar el input de salto
            if (states._jumpVariables.JumpUpVariable)
                //Se interrumpe la suma de velocidades extras en el salto hasta realizar un salto nuevo (Ya sea el primer salto o el segundo, si está activado el salto doble)
                states._jumpVariables.JumpHoldBreakUp = true;
        }
    }
}