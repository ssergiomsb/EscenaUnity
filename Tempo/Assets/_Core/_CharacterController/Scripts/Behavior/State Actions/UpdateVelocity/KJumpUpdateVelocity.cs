﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Controla la velocidad de salto del character. 
    /// </summary>
    /// <remarks>
    /// Nota: La distancia/altura de salto debe ser siempre la misma, para no afectar al diseño de niveles
    /// </remarks>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Velocity/Kinematic Jump Update Velocity")]
    public class KJumpUpdateVelocity : StateActions
    {
        /// <summary>
        /// Indica si está permitido realizar un doble salto
        /// </summary>
        [SerializeField]
        private bool _allowDoubleJump = false;
        /// <summary>
        /// Indica si se quiere resetear la velocidad en el eje Y cuando se realiza un salto
        /// </summary>
        /// <remarks>
        /// Al activarla la altura de todos los saltos será siempre la misma, ya que no hay ninguna velocidad previa que se sume o reste a la del salto
        /// Gameplay: Transmite una mejor sensación al jugador en casos en los que el character cae a una gran velocidad, y la fuerza del salto no contraresta adecuadamente esta velocidad previa
        /// </remarks>
        [SerializeField]
        private bool _resetYSpeedWhenJump = false;
        /// <summary>
        /// Velocidad que se le aplica al primer salto
        /// </summary>
        [SerializeField]
        private float _firstJumpSpeed = 10.0f;
        /// <summary>
        /// velocidad que se le aplica al segundo salto
        /// </summary>
        [SerializeField]
        private float _secondJumpSpeed = 12.0f;

        public override void Execute(StateManager states)
        {
            states._jumpVariables.JumpedThisFrame = false;
            states._jumpVariables.TimeSinceJumpRequested += states.DeltaTimeMotor;

            //Si se ha solicitado un salto
            if (states._jumpVariables.JumpRequested)
            {
                if (states._jumpVariables.FirstJumpConsumed)
                {
                    // Comprueba si se permite realizar un segundo salto
                    if (_allowDoubleJump && !states._jumpVariables.DoubleJumpConsumed && (states._jumpVariables.AllowJumpingWhenSliding ? !states._motor.GroundingStatus.FoundAnyGround : !states._motor.GroundingStatus.IsStableOnGround))
                    {
                        states._motor.ForceUnground();

                        if (_resetYSpeedWhenJump)
                            states._currentVelocityMotor.y = 0.0f;

                        states._currentVelocityMotor += (states._motor.CharacterUp * _secondJumpSpeed) - Vector3.Project(states._currentVelocityMotor, states._motor.CharacterUp);
                        states._jumpVariables.JumpRequested = false;
                        states._jumpVariables.DoubleJumpConsumed = true;
                        states._jumpVariables.JumpedThisFrame = true;
                        //Si se realiza un segundo salto, no se considera que se haya interrumpido el primer salto. Se usa para que se le siga aplicando la velocidad extra al salto en el caso de que tenga una (Por mantener el botón de salto pulsado)
                        states._jumpVariables.JumpHoldBreakUp = false;
                    }
                }

                // Comprueba si se permite realizar el primer salto
                if (!states._jumpVariables.FirstJumpConsumed)
                {
                    Vector3 jumpDirection;

                    //Si está sobre un terreno inestable, se usa el vector normal del suelo para establecer la dirección del sueño
                    if (states._motor.GroundingStatus.FoundAnyGround && !states._motor.GroundingStatus.IsStableOnGround)
                        jumpDirection = states._motor.GroundingStatus.GroundNormal;
                    else
                        jumpDirection = states._motor.CharacterUp;

                    // Impide que el character pueda quedarse pegado al suelo en el siguiente update, forzándolo a que el mismo se separe del suelo
                    states._motor.ForceUnground();
                    
                    if (_resetYSpeedWhenJump)
                        states._currentVelocityMotor.y = 0.0f; //Resetea a 0 la velocidad previa que hubiera en el eje Y

                    //Añade la velocidad del salto en la dirección establecida
                    states._currentVelocityMotor += (jumpDirection * _firstJumpSpeed) - Vector3.Project(states._currentVelocityMotor, states._motor.CharacterUp);

                    states._jumpVariables.JumpRequested = false;
                    states._jumpVariables.FirstJumpConsumed = true;
                    states._jumpVariables.JumpedThisFrame = true;
                }
            }
        }
    }
}