﻿using SO;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Controla la velocidad de movimiento del character
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Update Velocity/Kinematic Movement")]
    public class KMovement : StateActions
    {

        [Header("Stable Movement")]
        /// <summary>
        /// Velocidad máxima del character cuando está corriendo sobre suelo estable
        /// </summary>
        [SerializeField]
        private float _maxStableRunSpeed = 13.0f;
        /// <summary>
        /// Velocidad máxima del character, cuando está andando sobre suelo estable
        /// </summary>
        [SerializeField]
        private float _maxStableWalkSpeed = 8.0f;
        /// <summary>
        /// Velocidad máxima que tiene actualmente el character (Según si está andando o corriendo)
        /// </summary>
        private float _currentMaxStableMoveSpeed;
        /// <summary>
        /// Define la suavidad con la que se interpola la velocidad de movimiento
        /// </summary>
        [SerializeField]
        private float _stableMovementSharpness = 15.0f;

        [Header("Air Movement")]
        /// <summary>
        /// La velocidad máxima del character en el aire se establece en base a la velocidad máxima que tenía el character antes de estar en el aire
        /// </summary>
        /// <remarks>
        /// Si estaba corriendo será igual a "_maxStableRunSpeed", y si estab andando será "_maxStableWalkSpeed"
        /// </remarks>
        private float _maxAirMoveSpeed = 10.0f;
        /// <summary>
        /// Velocidad de aceleración estando en el aire
        /// </summary>
        [SerializeField]
        private float _airAccelerationSpeed = 15.0f;
        /// <summary>
        /// Fuerza de rozamiento en el aire
        /// </summary>
        /// <remarks>
        /// Se puede poner un valor mayor o menor para implementar varios tipos de salto en base a nuestras necesidades
        /// </remarks>
        [SerializeField]
        private float _airDrag = 1.0f;

        /// <summary>
        /// Fuerza de rozamiento del character cuando está sobre el suelo y recibiendo inputs de movimiento
        /// </summary>
        /// <remarks>
        /// Cuando el character está recibiendo inputs de entrada para que se mueva, lo normal es que tenga una fuerza de rozamiento menor
        /// </remarks>
        [SerializeField]
        private float _groundDragWithInputs = 0.1f;
        /// <summary>
        /// Fuerza de rozamiento del character cuando está sobre el suelo sin recibir inputs de movimiento
        /// </summary>
        /// <remarks>
        /// Si no está recibiendo ningín input de movimiento, podemos ajustar este valor para que el character frene mas en seco o de forma mas suave en base a la experiencia que se le quiera transmitir al jugador
        /// </remarks>
        [SerializeField]
        private float _groundDragWithoutInputs = 4.0f;

        /// <summary>
        /// Fuerza de rozamiento actual del character cuando está sobre el suelo
        /// </summary>
        private float _currentGroundDrag;

        public override void Execute(StateManager states)
        {
            //Si se están recibiendo inputs de movimiento
            if (states._movementVariables.MovementeAmount > 0.1f)
            {
                _currentGroundDrag = _groundDragWithInputs;

            } else {
                _currentGroundDrag = _groundDragWithoutInputs;
            }

            // Si se encuentra sobre suelo estable
            if (states._motor.GroundingStatus.IsStableOnGround)
            {
                float currentVelocityMagnitude = states._currentVelocityMotor.magnitude;

                Vector3 effectiveGroundNormal = states._motor.GroundingStatus.GroundNormal;

                //Investigar cuando se da el caso en el que haya que prevenir el snap
                //Si está en movimiento, y si debe prevenirse el snap sobre el suelo actual -- Snap = pegar/ajustar la posición de un objeto sobre algo(sueperficie, vértice, etc), el character en este caso
                if (currentVelocityMagnitude > 0.0f && states._motor.GroundingStatus.SnappingPrevented)
                {
                    //Obtiene las normales de donde venimos
                    //Las calcula entre la posición transitoria de la que viene el character, y el punto del suelo sobre el que se encuentra el character
                    Vector3 groundPointToCharacter = states._motor.TransientPosition - states._motor.GroundingStatus.GroundPoint;

                    //Si está por encima
                    if (Vector3.Dot(states._currentVelocityMotor, groundPointToCharacter) >= 0.0f)
                    {
                        effectiveGroundNormal = states._motor.GroundingStatus.OuterGroundNormal;
                        //Debug.Log("OuterGroundNormal" + effectiveGroundNormal);

                    } else {
                        effectiveGroundNormal = states._motor.GroundingStatus.InnerGroundNormal;
                        //Debug.Log("InnerGroundNormal" + effectiveGroundNormal);
                    }
                }

                // Reorienta la velocidad en las pendientes. 
                states._currentVelocityMotor = states._motor.GetDirectionTangentToSurface(states._currentVelocityMotor, effectiveGroundNormal) * currentVelocityMagnitude;
                // Calcula la target velocity
                //A partir de las normales del suelo y el vector right del character, se calcula el vector forward del personaje reorientado para que vaya paralelo al suelo y no pierda velocidad

                //Ajusta la velocidad máxima actual, según si el character está corriendo o andando
                if (states._movementVariables.Run)
                    _maxAirMoveSpeed = _currentMaxStableMoveSpeed = _maxStableRunSpeed;
                else
                    _maxAirMoveSpeed = _currentMaxStableMoveSpeed = _maxStableWalkSpeed;

                Vector3 inputRight = Vector3.Cross(states._movementVariables.TargetDirection, states._motor.CharacterUp);
                Vector3 reorientedInput = Vector3.Cross(effectiveGroundNormal, inputRight).normalized;
                Vector3 targetMovementVelocity = reorientedInput * _currentMaxStableMoveSpeed * states._movementVariables.MovementeAmount;

                // Interpola la velocidad de movimiento entre la actual y la objetivo
                states._currentVelocityMotor = Vector3.Lerp(states._currentVelocityMotor, targetMovementVelocity, 1.0f - Mathf.Exp(-_stableMovementSharpness * states.DeltaTimeMotor));


                // Add some gravity if we're grounded but not snapping to ground
                // (this situation can happen when we want to "launch off" ledges or declining slopes)
                //if (Motor.GroundingStatus.IsPreventingSnapping)
                //{
                //  states._currentVelocityMotor += states._gravity * states._deltaTimeMotor;
                //}

                // Aplica el drag a la velocidad de movimiento
                states._currentVelocityMotor *= (1.0f / (1.0f + (_currentGroundDrag * states.DeltaTimeMotor)));

            //Si está en el aire cayendo
            } else {
                // Si se está recibiendo algún input de movimiento
                if (states._movementVariables.MovementeAmount > 0.0f)
                {
                    Vector3 addedVelocity = states._movementVariables.TargetDirection * _airAccelerationSpeed * states.DeltaTimeMotor;

                    // Prevent air movement from making you move up steep sloped walls
                    if (states._motor.GroundingStatus.FoundAnyGround)
                    {
                        Vector3 perpenticularObstructionNormal = Vector3.Cross(Vector3.Cross(states._motor.CharacterUp, states._motor.GroundingStatus.GroundNormal), states._motor.CharacterUp).normalized;
                        addedVelocity = Vector3.ProjectOnPlane(addedVelocity, perpenticularObstructionNormal);
                    }

                    // Limit air movement from inputs to a certain maximum, without limiting the total air move speed from momentum, gravity or other forces
                    Vector3 resultingVelOnInputsPlane = Vector3.ProjectOnPlane(states._currentVelocityMotor + addedVelocity, states._motor.CharacterUp);
                    if (resultingVelOnInputsPlane.magnitude > _maxAirMoveSpeed && Vector3.Dot(states._motor.CharacterForward, resultingVelOnInputsPlane) >= 0f)
                    {
                        addedVelocity = Vector3.zero;
                    } else {
                        Vector3 velOnInputsPlane = Vector3.ProjectOnPlane(states._currentVelocityMotor, states._motor.CharacterUp);
                        Vector3 clampedResultingVelOnInputsPlane = Vector3.ClampMagnitude(resultingVelOnInputsPlane, _maxAirMoveSpeed);
                        addedVelocity = clampedResultingVelOnInputsPlane - velOnInputsPlane;
                    }

                    states._currentVelocityMotor += addedVelocity;
                }

                // Añade la gravedad
                states._currentVelocityMotor += states.Gravity * states.DeltaTimeMotor;

                // Aplica el drag a la velocidad de movimiento
                states._currentVelocityMotor *= (1f / (1f + (_airDrag * states.DeltaTimeMotor)));
            }
        }
    }
}