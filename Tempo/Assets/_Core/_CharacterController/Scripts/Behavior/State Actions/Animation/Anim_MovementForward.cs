﻿using UnityEngine;

namespace SA
{
	/// <summary>
	/// Controla la animación de movimiento del character
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/State Actions/Animations/Anim_MovementForward")]
	public class Anim_MovementForward : StateActions
	{
		public StateActions[] stateActions;

		public override void Execute(StateManager states)
		{
			if (stateActions != null)
			{
				for (int i = 0; i < stateActions.Length; i++)
				{
					stateActions[i].Execute(states);
				}
			}

			states.CharacterAnimator.SetFloat(states.characterHashes._anim.vertical_Float, states._movementVariables.MovementeAmount,0.2f, states.DeltaTimeMono);
		}

	}
}