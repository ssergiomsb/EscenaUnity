﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Actualiza el estado de la animación de caida
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Animations/Anim_Falling")]
    public class Anim_Falling : StateActions
    {

        public override void Execute(StateManager states)
        {
            states.CharacterAnimator.SetBool(states.characterHashes._anim.isGrounded_Bool, states._motor.GroundingStatus.IsStableOnGround);
        }
    }
}