﻿using UnityEngine;

namespace SA
{
	/// <summary>
	/// Activa/Desactiva el "Apply Root Motion" del Animator
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/State Actions/Set Root Motion Application")]
	public class SetRootMotionApplication : StateActions
	{
		public bool status;

		public override void Execute(StateManager states)
		{
			states.CharacterAnimator.applyRootMotion = status;
		}

	}
}
