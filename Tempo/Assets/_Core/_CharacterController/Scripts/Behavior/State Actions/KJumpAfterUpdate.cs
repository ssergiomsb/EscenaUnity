﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Controla el movimiento hacia delante del personaje
    /// </summary>
    [CreateAssetMenu(menuName = "Actions/State Actions/Kinematic Jump After Update")]
    public class KJumpAfterUpdate : StateActions
    {
        /// <summary>
        /// Tiempo antes de aterrizar sobre el suelo, durante el cual se guardaran los inputs de salto que se hayan recibido para aplicarlos inmeditamente al aterrizar
        /// </summary>
        /// <remarks>
        /// Gameplay: Da una mejor sensación al jugador al no tener que ser tan preciso cuando pulsa el botón de salto, aunque lo pulse antes de tiempo notará que el character le responde mejor
        /// </remarks>
        [SerializeField]
        private float _jumpPreGroundingGraceTime = 0.0f;

        public override void Execute(StateManager states)
        {
            // Maneja el tiempo de gracia antes de aterrizar
            if (states._jumpVariables.JumpRequested && states._jumpVariables.TimeSinceJumpRequested > _jumpPreGroundingGraceTime)
                states._jumpVariables.JumpRequested = false;

            /*if (!states._motor.GroundingStatus.IsStableOnGround)
                states._jumpVariables.TimeSinceLastAbleToJump += states._deltaTimeMotor;*/
        }
    }
}