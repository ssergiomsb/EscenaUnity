﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
	/// <summary>
	/// Action usada para ejecutar otra lista de Actions 
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/Mono Actions/Action Batch")]
	public class ActionBatch : Action
	{
		public Action[] _actions;

		public override void Execute()
		{
			for (int i = 0; i < _actions.Length; i++)
			{
				_actions[i].Execute();
			}
		}
	}
}