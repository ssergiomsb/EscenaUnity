﻿using UnityEngine;
using SO;

namespace SA
{
	/// <summary>
	/// Guarda el valor obtenido de un Axis en una FloatVariable
	/// </summary>
    [CreateAssetMenu(menuName = "Inputs/Axis")]
    public class InputAxis : Action
    {
		[SerializeField]
		private string targetString = default;
		[SerializeField]
		private float _value;

		//Guardando el valor del input en una variable, podremos acceder a el desde cualquier otro script
		[SerializeField]
		private FloatVariable variable = default;

        public override void Execute()
        {
            _value = Input.GetAxis(targetString);

			if (variable != null)
				variable.value = _value;
        }
    }
}
