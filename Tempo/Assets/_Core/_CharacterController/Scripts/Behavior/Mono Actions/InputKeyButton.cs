﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
	/// <summary>
	/// 
	/// </summary>
	[CreateAssetMenu(menuName = "Inputs/KeyButton")]
	public class InputKeyButton : Action
	{
		public KeyCode keyCode;
		private bool isPressed;
		public KeyState keyState;
		public bool updateBoolVar = true;
		
		public SO.BoolVariable targetBoolVariable;


		public override void Execute()
		{
			switch (keyState)
			{
				case KeyState.onDown:
					isPressed = Input.GetKeyDown(keyCode);
					break;
				case KeyState.onCurrent:
					isPressed = Input.GetKey(keyCode);
					break;
				case KeyState.onUp:
					isPressed = Input.GetKeyUp(keyCode);
					break;
				default:
					break;
			}

			if (updateBoolVar)
			{
				if (targetBoolVariable != null)
				{
					targetBoolVariable.value = isPressed;
				}
			}
		}

		public enum KeyState
		{
			onDown,onCurrent,onUp
		}
	}
}
