﻿using UnityEngine;
using SO;

namespace SA
{
	/// <summary>
	/// Controla los inputs del juego
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/Mono Actions/InputManager")]
	public class InputManager : Action
	{
        [Header("Movement")]
        [SerializeField]
		private FloatVariable _horizontal = default;
		[SerializeField]
		private FloatVariable _vertical = default;
        [SerializeField]
        private BoolVariable _run = default;

        [Header("Jump")]
        [SerializeField]
		private BoolVariable _jumpDown = default;
        [SerializeField]
        private BoolVariable _jumpCurrent = default;
        [SerializeField]
        private BoolVariable _jumpUp = default;

        [Header("Others")]
        [SerializeField]
        private ActionBatch _inputUpdateBatch = default;
        [SerializeField]
        private StateManagerVariable _playerStates = default;

        public override void Execute()
		{
			_inputUpdateBatch.Execute();

			if (_playerStates.Value != null)
			{

                    /* MOVEMENT */
                _playerStates.Value._movementVariables.Horizontal = _horizontal.value;
				_playerStates.Value._movementVariables.Vertical = _vertical.value;

				float moveAmount = Mathf.Clamp01((Mathf.Abs(_horizontal.value) + Mathf.Abs(_vertical.value)));
				_playerStates.Value._movementVariables.MovementeAmount = moveAmount;

                _playerStates.Value._movementVariables.Run = _run.value;

                    /* JUMP */
                _playerStates.Value._jumpVariables.JumpCurrentVariable = _jumpCurrent.value;

                //Si se pulsa el input de salto
                if (_jumpDown.value)
				    _playerStates.Value._jumpVariables.JumpRequested = _jumpDown.value;

                //Si se deja de pulsar el input de salto
                _playerStates.Value._jumpVariables.JumpUpVariable = _jumpUp.value;

            }
		}
	}
}