﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SO;

namespace SA
{
	/// <summary>
	/// Actualiza el DeltaTime almacenado en una FloatVariable
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/Mono Actions/Delta Time Manager")]
	public class DeltaTimeManager : Action
	{
		[SerializeField]
		private FloatVariable targetVariable = default;

		public override void Execute()
		{
			targetVariable.value = Time.deltaTime;
		}

	}
}