﻿using UnityEngine;
using SO;

namespace SA
{
	/// <summary>
	/// Permite que un objeto siga la posición de otro
	/// </summary>
	[CreateAssetMenu(menuName = "Actions/Mono Actions/Follow Transform")]
	public class FollowTransform : Action
	{
		[SerializeField]
		private TransformVariable targetTransform = default;
		[SerializeField]
		private TransformVariable currentTransform = default;
		[SerializeField]
		private FloatVariable delta = default;

		[SerializeField]
		private float speed = 9;

		public override void Execute()
		{
			if (targetTransform.value == null)
				return;
			if (currentTransform.value == null)
				return;

			Vector3 targetPosition = Vector3.Lerp(currentTransform.value.position, targetTransform.value.position,
				delta.value * speed);
			currentTransform.value.position = targetPosition;
		}
	}
}