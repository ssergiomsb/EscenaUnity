﻿using KinematicCharacterController;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// Controla los estados del player
    /// </summary>
    public class StateManager : MonoBehaviour, ICharacterController
    {
        [Header("Kinematic Character Controller")]
        /// <summary>
        /// Kinematic Character Motor
        /// </summary>
        /// <remarks>
        /// Nota: Debe permanecer como una variable pública por requisitos del motor
        /// </remarks>
        public KinematicCharacterMotor _motor;
        private float _deltaTimeMotor;
        /// <summary>
        /// Cashea la rotación actual en el motor, durante cada pasada realizada por el motor para actualizar la rotación
        /// </summary>
        /// <remarks>
        /// Las StateAction que actualizan la rotación, lo hacen a través de esta variable ejecutandose en un determinado orden
        /// Nota: Debe permanecer como una variable pública por requisitos del motor
        /// </remarks>
        [HideInInspector]
        public Quaternion _currentRotationMotor;
        /// <summary>
        /// Cashea la velocidad actual en el motor, durante cada pasada realizada por el motor para actualizar la velocidad
        /// </summary>
        /// <remarks>
        /// Las StateAction que actualizan la velocidad, lo hacen a través de esta variable ejecutandose en un determinado orden
        /// Nota: Debe permanecer como una variable pública por requisitos del motor
        /// </remarks>
        [HideInInspector]
        public Vector3 _currentVelocityMotor;
        /// <summary>
        /// Gravedad
        /// </summary>
        private Vector3 _gravity = new Vector3(0, -30f, 0);

        /// <summary>
        /// Estado inicial del character
        /// </summary>
        [SerializeField]
        private State _initialState = default;
        /// <summary>
        /// Estado actual del character
        /// </summary>
        private State _currentState;

        /// <summary>
        /// Variables que controlan el movimiento del jugador
        /// </summary>
        /// <remarks>
        /// Nota: Dejarla pública
        /// </remarks>
        public MovementVariables _movementVariables;
        /// <summary>
        /// Variables para controlar el salto
        /// </summary>
        /// <remarks>
        /// Nota: Dejarla pública
        /// </remarks>
        public JumpVariables _jumpVariables;

        /// <summary>
        /// Deltatime del monobehaviour
        /// </summary>
        private float _deltaTimeMono;

        /// <summary>
        /// Transform del character
        /// </summary>
        private Transform _characterTransform;
        /// <summary>
        /// Animator del character controller
        /// </summary>
		private Animator _characterAnimator;
        /// <summary>
        /// Collider del character controller
        /// </summary>
        private Collider _characterCollider;

        public CharacterHashes characterHashes;

        /*public bool _isJumping;
        public bool _isGrounded;
        public bool _isVaulting;*/

        public State CurrentState
        {
            get { return _currentState; }
            set { _currentState = value; }
        }

        public float DeltaTimeMotor
        {
            get { return _deltaTimeMotor; }
            set { _deltaTimeMotor = value; }
        }

        public Vector3 Gravity
        {
            get { return _gravity; }
            set { _gravity = value; }
        }

        public float DeltaTimeMono
        {
            get { return _deltaTimeMono; }
            set { _deltaTimeMono = value; }
        }

        public Transform CharacterTransform
        {
            get { return _characterTransform; }
            set { _characterTransform = value; }
        }

        public Animator CharacterAnimator
        {
            get { return _characterAnimator; }
            set { _characterAnimator = value; }
        }

        public Collider CharacterCollider
        {
            get { return _characterCollider; }
            set { _characterCollider = value; }
        }

        private void Start()
        {
            _currentState = _initialState;

            _characterTransform = this.transform;

            _characterCollider = GetComponent<Collider>();

			_characterAnimator = GetComponentInChildren<Animator>();

			characterHashes = new CharacterHashes();

            if(_motor != null)
                _motor.CharacterController = this;
        }

		private void FixedUpdate()
		{
			_deltaTimeMono = Time.fixedDeltaTime;

			if (_currentState != null)
			{
				_currentState.FixedTick(this);
			}
		}

		private void Update()
        {
			_deltaTimeMono = Time.deltaTime;

            if(_currentState != null)
            {
                _currentState.Tick(this);
            }
        }

        public void BeforeCharacterUpdate(float deltaTime)
        {
            // This is called before the motor does anything
        }

        public void UpdateRotation(ref Quaternion currentRotation, float deltaTime)
        {
            // This is called when the motor wants to know what its rotation should be right now
            _currentRotationMotor = currentRotation;
            _deltaTimeMotor = deltaTime;

            if (_currentState != null)
            {
                _currentState.UpdateRotationTick(this);
            }

            currentRotation = _currentRotationMotor;
        }

        public void UpdateVelocity(ref Vector3 currentVelocity, float deltaTime)
        {
            // This is called when the motor wants to know what its velocity should be right now
            _currentVelocityMotor = currentVelocity;
            _deltaTimeMotor = deltaTime;

            ///
            if (_currentState != null)
            {
                _currentState.UpdateVelocityTick(this);
            }

            currentVelocity = _currentVelocityMotor;
        }

        public void AfterCharacterUpdate(float deltaTime)
        {
            // This is called after the motor has finished everything in its update
            _deltaTimeMotor = deltaTime;

            if (_currentState != null)
            {
                _currentState.AfterCharacterUpdateTick(this);
            }
        }

        public bool IsColliderValidForCollisions(Collider coll)
        {
            // This is called after when the motor wants to know if the collider can be collided with (or if we just go through it)
            return true;
        }

        public void OnGroundHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport)
        {
            // This is called when the motor's ground probing detects a ground hit
        }

        public void OnMovementHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport)
        {
            // This is called when the motor's movement logic detects a hit
        }

        public void ProcessHitStabilityReport(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport hitStabilityReport)
        {
            // This is called after every hit detected in the motor, to give you a chance to modify the HitStabilityReport any way you want
        }

        public void PostGroundingUpdate(float deltaTime)
        {
            // This is called after the motor has finished its ground probing, but before PhysicsMover/Velocity/etc.... handling
        }

        public void OnDiscreteCollisionDetected(Collider hitCollider)
        {
            // This is called by the motor when it is detecting a collision that did not result from a "movement hit".
        }
    }
}