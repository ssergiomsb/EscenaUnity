﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Contiene todas hashes utilizadas necesarias del character
    /// </summary>
    public class CharacterHashes
    {
        public AnimHashes _anim;
        public AnimConstant _animConstant;

        public CharacterHashes()
        {
            _anim = new AnimHashes();
            _animConstant = new AnimConstant();
        }
    }


	/// <summary>
	/// Contiene una serie de hashes para los parámetros de animación del character
	/// </summary>
	public class AnimHashes
	{
		public int vertical_Float = Animator.StringToHash("vertical");
		public int horizontal_Float = Animator.StringToHash("horizontal");

        public int isGrounded_Bool = Animator.StringToHash("isGrounded");
        public int isLedgeClim_Bool = Animator.StringToHash("isLedgeClimb");
               
        public int jumpForward_State = Animator.StringToHash("Jump Forward");

        

        public int empty_State = Animator.StringToHash("Empty");


        /*
        public int ledgeClimb = Animator.StringToHash("Ledge Climb");
        public int jumpIdle_State = Animator.StringToHash("Jump Idle");
        
        public int isInteracting = Animator.StringToHash("isInteracting");
        public int LandFast = Animator.StringToHash("Land Fast");
        public int LandHard = Animator.StringToHash("Land Hard");
        public int LandRolling = Animator.StringToHash("Land Rolling");
        public int VaultWalk = Animator.StringToHash("Vault Walk");
        
         */
    }

    public class StateConstant
    {
        public const string LOCOMOTION = "Locomotion";
        public const string JUMPING = "Jumping";
        public const string LEDGE_CLIMB = "LedgeClimb";
    }

    public class AnimConstant
    {

        public const string LAYER_LOCOMOTION = "Locomotion Layer";
        public const string LAYER_OVERRIDE_ALL = "Override All Layer";

        public const string ANIM_JUMP_FORWARD = "Jump Forward";

        public const string PARAM_COLLIDER_HEIGHT = "ColliderHeight";
        public const string PARAM_COLLIDER_Y= "ColliderY";
    }

    public class CharacterMotorConstant
    {
        public const float CAPSULE_COLLIDER_HEIGHT = 2.1f;
        public const float CAPSULE_COLLIDER_Y = 0.9f;
        public const float CAPSULE_RADIUS = 0.45f;
    }
}