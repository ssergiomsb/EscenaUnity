﻿using UnityEngine;

namespace SA
{
    /// <summary>
    /// Almacena el valor de todas las variables de movimiento
    /// </summary>
    [System.Serializable]
    public class MovementVariables
    {
        /// <summary>
        /// Valor del input en el eje horizontal
        /// </summary>
        private float _horizontal;
        /// <summary>
        /// Valor del input en el eje vertical
        /// </summary>
        private float _vertical;
        /// <summary>
        /// Indica si el jugador está corriendo
        /// </summary>
        private bool _run;
        /// <summary>
        /// Cantidad de movimiento en base a la suma de los dos ejes (Horizontal y Vertical)
        /// </summary>
        /// <remarks>Se utiliza para determinar la velocidad de movimiento</remarks>
        private float _movementAmount;
        /// <summary>
        /// Dirección hacia la que se tiene que mover el character
        /// </summary>
        private Vector3 _targetDirection;
        /// <summary>
        /// Se usa para mover el character en base a una posición determinada
        /// </summary>
        /// <remarks>
        /// Para determinados "states" en los que la posición del jugador deba modificarse de forma mas controlada/precisa
        /// Ejemplo: Agarrarse a una borde, subir escaleras, trepar por una cuerda, desplazarse a un punto distante donde se haya enganchado, etc
        /// </remarks>
        private Vector3 _targetPosition;

        public float Horizontal
        {
            get { return _horizontal; }
            set { _horizontal = value; }
        }

        public float Vertical
        {
            get { return _vertical; }
            set { _vertical = value; }
        }

        public float MovementeAmount
        {
            get { return _movementAmount; }
            set { _movementAmount = value; }
        }

        public Vector3 TargetDirection
        {
            get { return _targetDirection; }
            set { _targetDirection = value; }
        }

        public Vector3 TargetPosition
        {
            get { return _targetPosition; }
            set { _targetPosition = value; }
        }

        public bool Run
        {
            get { return _run; }
            set { _run = value; }
        }
    }

    /// <summary>
    /// Almacena el valor de todas las variables del salto
    /// </summary>
    [System.Serializable]
    public class JumpVariables
    {
        [Header("Jumping")]
        /// <summary>
        /// Indica si está permitido saltar cuando el character se está deslizando sobre una superficie, aunque no esté sobre suelo estable
        /// </summary>
        [SerializeField]
        private bool _allowJumpingWhenSliding = false;
        /// <summary>
        /// Valor actual de la variable/input de salto
        /// </summary>
        private bool _jumpCurrentVariable = false;
        /// <summary>
        /// Indica si se ha dejado de pulsar el input de salto
        /// </summary>
        private bool _jumpUpVariable = false;
        /// <summary>
        /// Si se ha solicitado realizar un salto (Puede venir tanto de un input del jugador, como de una IA que requiera saltar por determinadas condiciones)
        /// </summary>
        private bool _jumpRequested = false;
        /// <summary>
        /// Indica si se ha consumido el primer salto
        /// </summary>
        private bool _firstJumpConsumed = false;
        /// <summary>
        /// Indica si se ha consumido el segundo salto
        /// </summary>
        private bool _doubleJumpConsumed = false;
        /// <summary>
        /// Indica si se ha dejado de pulsar el input de salto durante el salto
        /// </summary>
        /// <remarks>
        /// Uso: Intenrumpir, por ejemplo, todas las velocidades que puedan estar sumándose de forma continua al mantener el botón pulsado(Salto sostenido), hasta que vuelva a recargarse el salto
        /// </remarks>
        private bool _jumpHoldBreakUp = false;

        /// <summary>
        /// Indica si se ha saltado en este frame
        /// </summary>
        /// <remarks>
        /// Uso: Se usa para evita que el character pueda dar por finalizado el salto el mismo frame que salta, porque todavía no le haya dado tiempo a separarse del suelo y considere que ha aterrizado ese mismo frame
        /// </remarks>
        private bool _jumpedThisFrame = false;
        /// <summary>
        /// Tiempo transcurrido desde que se solicito realizar el salto
        /// </summary>
        private float _timeSinceJumpRequested = Mathf.Infinity;
        /// <summary>
        /// Tiempo transcurrido desde la última vez que estuvo disponible el salto
        /// </summary>
        private float _timeSinceLastAbleToJump = 0.0f;
        /// <summary>
        /// Indica si actualmente está saltando
        /// </summary>
        private bool _isJumping;
        /// <summary>
        /// Tiempo transcurrido desde que se inició el salto
        /// </summary>
        private float _currentTimeJumping = 0.0f;
        
        public bool AllowJumpingWhenSliding
        {
            get { return _allowJumpingWhenSliding; }
            set { _allowJumpingWhenSliding = value; }
        }

        /// <summary>
        /// Indica si se ha dejado de pulsar el input de salto
        /// </summary>
        public bool JumpUpVariable
        {
            get { return _jumpUpVariable; }
            set { _jumpUpVariable = value; }
        }

        /// <summary>
        /// Valor actual de la variable de salto, que contiene el valor actual del input de salto
        /// </summary>
        public bool JumpCurrentVariable
        {
            get { return _jumpCurrentVariable; }
            set { _jumpCurrentVariable = value; }
        }

        /// <summary>
        /// Propiedad utilizada para solicitar que el character realice un salto
        /// </summary>
        public bool JumpRequested
        {
            get { return _jumpRequested; }
            set
            {
                if(value)
                    TimeSinceJumpRequested = 0.0f;

                _jumpRequested = value;
            }
        }

        /// <summary>
        /// Indica si se ha dejado de pulsar el input de salto durante el salto
        /// </summary>
        public bool JumpHoldBreakUp
        {
            get { return _jumpHoldBreakUp; }
            set { _jumpHoldBreakUp = value; }
        }

        /// <summary>
        /// Indica si ha saltado este mismo frame
        /// </summary>
        public bool JumpedThisFrame
        {
            get { return _jumpedThisFrame; }
            set { _jumpedThisFrame = value; }
        }

        /// <summary>
        /// Tiempo transcurrido desde que se solicito realizar un salto
        /// </summary>
        /// <remarks> Se restablece a 0 al actualizar el valor de la propiedad JumpRequested </remarks>
        public float TimeSinceJumpRequested
        {
            get { return _timeSinceJumpRequested; }
            set { _timeSinceJumpRequested = value; }
        }

        /// <summary>
        /// Tiempo transcurrido desde la última vez que estuvo disponible el salto
        /// </summary>
        public float TimeSinceLastAbleToJump
        {
            get { return _timeSinceLastAbleToJump; }
            set { _timeSinceLastAbleToJump = value; }
        }

        /// <summary>
        /// Indica si se ha consumido el primer salto
        /// </summary>
        public bool FirstJumpConsumed
        {
            get { return _firstJumpConsumed; }
            set { _firstJumpConsumed = value; }
        }

        /// <summary>
        /// Indica si se ha consumido el segundo salto
        /// </summary>
        public bool DoubleJumpConsumed
        {
            get { return _doubleJumpConsumed; }
            set { _doubleJumpConsumed = value; }
        }

        public bool IsJumping
        {
            get { return _isJumping; }
            set { _isJumping = value; }
        }

        public float CurrentTimeJumping
        {
            get { return _currentTimeJumping; }
            set { _currentTimeJumping = value; }
        }

        /// <summary>
        /// Restablece los valores por defecto de las variables de salto
        /// </summary>
        public void RestoreJump()
        {
            _firstJumpConsumed = false;
            _doubleJumpConsumed = false;
            _isJumping = false;

            CurrentTimeJumping = 0.0f;
            _jumpHoldBreakUp = false;

            TimeSinceLastAbleToJump = 0.0f;
        }
    }
}