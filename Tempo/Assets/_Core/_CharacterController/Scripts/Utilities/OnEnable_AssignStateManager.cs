﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SA
{
	public class OnEnable_AssignStateManager : MonoBehaviour
	{

		public StateManagerVariable _stateManagerVariable;

		private void OnEnable()
		{
			_stateManagerVariable.Value = GetComponent<StateManager>();
			Destroy(this);
		}
	}
}