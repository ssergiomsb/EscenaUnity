﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
	[CreateAssetMenu(menuName = "Variables/StateManager")]
	public class StateManagerVariable : ScriptableObject
	{
		public StateManager _stateManager;

		public StateManager Value
		{
			get { return _stateManager; }
			set { _stateManager = value; }
		}
	}
}