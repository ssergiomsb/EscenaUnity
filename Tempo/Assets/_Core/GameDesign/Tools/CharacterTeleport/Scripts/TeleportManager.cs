﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

namespace GD_Tools
{
    [System.Serializable]
    public class TeleportLocation
    {
        public string _description;
        public Transform _location;
        public KeyCode _key;
    }

    public class TeleportManager : MonoBehaviour
    {
        [SerializeField]
        private StateManagerVariable _stateManagerVariable = default;
        [SerializeField]
        private TeleportLocation[] _teleportLocations = default;

        void Update()
        {
            for(int i = 0; i < _teleportLocations.Length; i++)
            {
                if (Input.GetKeyDown(_teleportLocations[i]._key))
                    _stateManagerVariable.Value._motor.SetPositionAndRotation(_teleportLocations[i]._location.position, _teleportLocations[i]._location.rotation);
            }
        }
    }
}
