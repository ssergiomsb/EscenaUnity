﻿using System.Collections.Generic;

using UnityEngine;

namespace WinterboltGames.Utilities
{
	public static class TransformUtilities
	{
		public static List<Transform> GetChildrenRecursive(Transform transform)
		{
			List<Transform> transforms = new List<Transform>() { transform, };

			for (int i = 0; i < transform.childCount; i++)
			{
				transforms.AddRange(GetChildrenRecursive(transform.GetChild(i)));
			}

			return transforms;
		}
	}
}
