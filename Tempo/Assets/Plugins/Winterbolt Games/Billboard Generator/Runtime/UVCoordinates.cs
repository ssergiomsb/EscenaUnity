﻿using System;

using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	[Serializable]
	public struct UVCoordinates
	{
		public Vector2 BottomLeft;
		public Vector2 BottomRight;

		public Vector2 TopRight;
		public Vector2 TopLeft;

		public UVCoordinates(float x, float y, float width, float height, float uvMapWidth, float uvMapHeight)
		{
			TopLeft = new Vector2(x / uvMapWidth, (y + height) / uvMapHeight);

			TopRight = new Vector2((x + width) / uvMapWidth, (y + height) / uvMapHeight);

			BottomLeft = new Vector2(x / uvMapWidth, y / uvMapHeight);

			BottomRight = new Vector2((x + width) / uvMapWidth, y / uvMapHeight);
		}
	}
}