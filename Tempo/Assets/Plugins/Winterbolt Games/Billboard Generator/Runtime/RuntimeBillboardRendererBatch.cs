﻿using System.Collections.Generic;

using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	public class RuntimeBillboardRendererBatch : MonoBehaviour
	{
		public int UpdatesPerBatch;

		public List<RuntimeBillboardRenderer> Billboards;

		private int _start;

		private void LateUpdate()
		{
			int end = _start + UpdatesPerBatch;

			for (int i = _start; i < end; i++)
			{
				Billboards[i].UpdateBillboard();
			}

			_start += UpdatesPerBatch;

			if (_start > Billboards.Count - 1)
			{
				_start = 0;
			}
		}
	}
}
