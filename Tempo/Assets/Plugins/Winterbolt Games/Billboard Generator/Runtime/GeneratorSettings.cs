﻿using System;

using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	[Serializable]
	public struct GeneratorSettings
	{
		[Range(1, 360)] public int Directions;

		public int TextureWidth;
		public int TextureHeight;

		public bool UseMainCamera;

		public Color TextureBackgroundColor;

		public bool UseOrthographicCamera;

		public float CameraSizeOrFieldOfView;

		public CameraClearFlags CameraClearFlags;

		public Vector3 CameraOffset;

		public bool RotateOnXAxis;
		public bool RotateOnYAxis;
		public bool RotateOnZAxis;
	}
}