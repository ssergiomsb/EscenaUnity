﻿using System.Collections.Generic;

using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	[CreateAssetMenu(menuName = "Billboard Generator/Billboard Atlas Asset")]
	public class BillboardAtlasAsset : ScriptableObject
	{
		public Vector3 RendererScale;

		public List<UVCoordinates> UVCoordinates;
	}
}