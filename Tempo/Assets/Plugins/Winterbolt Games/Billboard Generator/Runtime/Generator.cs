﻿using System;

using UnityEngine;

using Object = UnityEngine.Object;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	public static class Generator
	{
		private static GameObject s_cameraGameObject;

		private static Camera s_camera;

		private static RenderTexture s_renderTexture;

		private static void Prepare(GeneratorSettings settings)
		{
			if (s_cameraGameObject != null)
			{
				if (Application.isEditor)
				{
					if (Application.isPlaying)
					{
						Object.Destroy(s_cameraGameObject);
					}
					else
					{
						Object.DestroyImmediate(s_cameraGameObject);
					}
				}
				else
				{
					Object.Destroy(s_cameraGameObject);
				}
			}

			if (settings.UseMainCamera)
			{
				if (Camera.main == null)
				{
					Debug.LogError("No camera tagged 'MainCamera' is present in the current scene.");
				}
				else
				{
					Camera mainCameraClone = Object.Instantiate(Camera.main);

					s_cameraGameObject = mainCameraClone.gameObject;

					s_camera = mainCameraClone;
				}
			}
			else
			{
				s_cameraGameObject = new GameObject("Billboard Camera");

				s_camera = s_cameraGameObject.AddComponent<Camera>();

				s_camera.clearFlags = settings.CameraClearFlags;

				s_camera.backgroundColor = settings.TextureBackgroundColor;

				s_camera.depth = -1;
			}

			if (s_renderTexture != null)
			{
				if (Application.isEditor)
				{
					if (Application.isPlaying)
					{
						Object.Destroy(s_renderTexture);
					}
					else
					{
						Object.DestroyImmediate(s_renderTexture);
					}
				}
				else
				{
					Object.Destroy(s_renderTexture);
				}
			}

			s_renderTexture = new RenderTexture(settings.TextureWidth, settings.TextureHeight, 24, RenderTextureFormat.Default) { name = "Billboard Render Texture", };

			s_renderTexture.Create();

			RenderTexture.active = s_renderTexture;

			s_camera.targetTexture = s_renderTexture;
		}

		private static void Progress(GameObject gameObject, GeneratorSettings settings, Action<Texture2D> progress = null)
		{
			if (settings.UseOrthographicCamera)
			{
				s_camera.orthographic = true;

				s_camera.orthographicSize = settings.CameraSizeOrFieldOfView;
			}
			else
			{
				s_camera.orthographic = false;

				s_camera.fieldOfView = settings.CameraSizeOrFieldOfView;
			}

			s_camera.transform.position = gameObject.transform.position + settings.CameraOffset;

			int loops = 360 / (360 / settings.Directions);

			int angle = 360 / loops;

			try
			{
				for (int i = 0; i < loops; i++)
				{
					s_camera.Render();

					Texture2D result = new Texture2D(settings.TextureWidth, settings.TextureHeight, TextureFormat.ARGB32, false, false) { name = string.Format("{0} Billboard {1} Angle {2} Texture", gameObject.name, i, i * angle), };

					result.ReadPixels(new Rect(0, 0, settings.TextureWidth, settings.TextureHeight), 0, 0);

					result.Apply();

					Vector3 axis = new Vector3(settings.RotateOnXAxis ? 1.0f : 0.0f, settings.RotateOnYAxis ? 1.0f : 0.0f, settings.RotateOnZAxis ? 1.0f : 0.0f);

					s_camera.transform.RotateAround(gameObject.transform.position, axis, angle);

					if (progress != null)
					{
						progress.Invoke(result);
					}
				}
			}
			catch (Exception exception)
			{
				Debug.LogError(exception.ToString());
			}

			CleanUp();
		}

		private static void CleanUp()
		{
			RenderTexture.active = null;

			if (s_camera != null)
			{
				s_camera.targetTexture = null;
			}

			if (s_cameraGameObject != null)
			{
				if (Application.isEditor)
				{
					if (Application.isPlaying)
					{
						Object.Destroy(s_cameraGameObject);
					}
					else
					{
						Object.DestroyImmediate(s_cameraGameObject);
					}
				}
				else
				{
					Object.Destroy(s_cameraGameObject);
				}
			}

			if (s_renderTexture != null)
			{
				if (Application.isEditor)
				{
					if (Application.isPlaying)
					{
						Object.Destroy(s_renderTexture);
					}
					else
					{
						Object.DestroyImmediate(s_renderTexture);
					}
				}
				else
				{
					Object.Destroy(s_renderTexture);
				}
			}
		}

		public static void Generate(GameObject gameObject, GeneratorSettings settings, Action<Texture2D> progress = null)
		{
			Prepare(settings);

			Progress(gameObject, settings, progress);
		}
	}
}
