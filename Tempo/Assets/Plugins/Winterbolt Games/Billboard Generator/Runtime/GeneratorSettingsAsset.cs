﻿
using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	[CreateAssetMenu(menuName = "Billboard Generator/Generator Settings Asset")]
	public class GeneratorSettingsAsset : ScriptableObject
	{
		public GeneratorSettings Settings;
	}
}