﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace WinterboltGames.Tools.BillboardGenerator.Runtime
{
	public class RuntimeBillboardRenderer : MonoBehaviour
	{
		[SerializeField] private Transform _target;

		[SerializeField]
		[Tooltip("If true, the billboard will need a MeshFilter and a MeshRenderer otherwise it will use Graphics.DrawMesh.")]
		private bool _useAlternativeWorkflow;

		private Mesh _mesh;

		[SerializeField] private Material _material;

		[SerializeField] private BillboardAtlasAsset _atlas;

		[SerializeField] private MeshFilter _meshFilter;

		[SerializeField] private MeshRenderer _meshRenderer;

		[SerializeField]
		[Tooltip("Disable this if the billboard is included in a batch.")]
		private bool _update;

		[SerializeField]
		[Tooltip("Disable this if the billboard is using the alternative workflow.")]
		private bool _render;

		private int _snap;

		private readonly Dictionary<int, int> _directions = new Dictionary<int, int>();

		private readonly Vector2[] _uvs = new Vector2[4];

		private static Mesh MakeQuad()
		{
			Mesh mesh = new Mesh() { name = "Billboard", };

			Vector3[] vertices = new Vector3[] { new Vector3(-0.5f, -0.5f), new Vector3(0.5f, -0.5f), new Vector3(0.5f, 0.5f), new Vector3(-0.5f, 0.5f), };

			mesh.vertices = vertices;

			int[] triangles = new int[] { 2, 1, 0, 3, 2, 0, };

			mesh.triangles = triangles;

			Vector2[] uv = new Vector2[] { new Vector2(0.0f, 0.0f), new Vector2(1.0f, 0.0f), new Vector2(1.0f, 1.0f), new Vector2(0.0f, 1.0f), };

			mesh.uv = uv;

			mesh.RecalculateBounds();
			mesh.RecalculateNormals();
			mesh.RecalculateTangents();

			mesh.MarkDynamic();

			return mesh;
		}

		private void Start()
		{
			if (_target == null)
			{
				_target = Camera.main.transform;
			}

			_mesh = MakeQuad();

			if (_useAlternativeWorkflow)
			{
				_meshFilter.mesh = _mesh;

				transform.localScale = _atlas.RendererScale;
			}

			_snap = 360 / _atlas.UVCoordinates.Count;

			for (int i = 0; i < 360 / _snap; i++)
			{
				_directions.Add(i * _snap, i);
			}
		}

		private void LateUpdate()
		{
			if (_update)
			{
				UpdateBillboard();
			}

			if (_render && !_useAlternativeWorkflow)
			{
				Graphics.DrawMesh(_mesh, Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale + _atlas.RendererScale), _material, gameObject.layer);
			}
		}

		public void UpdateBillboard()
		{
			Transform cachedTransform = transform;

			Vector3 cachedPosition = cachedTransform.position;

			Vector3 direction = cachedPosition - _target.position;

			Vector3 angleToTarget = Quaternion.LookRotation(direction).eulerAngles;

			int angle = Mathf.CeilToInt(angleToTarget.y / _snap) * _snap;

			if (_directions.TryGetValue(angle != 0 && angle != 360 ? angle : 0, out int index))
			{
				UVCoordinates uvs = _atlas.UVCoordinates[index];

				_uvs[0] = uvs.BottomLeft;
				_uvs[1] = uvs.BottomRight;
				_uvs[2] = uvs.TopRight;
				_uvs[3] = uvs.TopLeft;
			}

			_mesh.uv = _uvs;

			cachedTransform.rotation = Quaternion.Euler(0.0f, angle, 0.0f);
		}
	}
}
