﻿using UnityEditor;

using UnityEngine;

using WinterboltGames.Tools.BillboardGenerator.Runtime;

namespace WinterboltGames.Tools.BillboardGenerator.Editor
{
	[CustomEditor(typeof(GeneratorSettingsAsset)), CanEditMultipleObjects]
	public class GeneratorSettingsAssetInspector : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			SerializedObject serializedObject = new SerializedObject(target as GeneratorSettingsAsset);

			SerializedProperty settingsProperty = serializedObject.FindProperty("Settings");

			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("Directions"));

			EditorGUILayout.HelpBox(string.Format("The maximum supported texture size by your system is {0}", SystemInfo.maxTextureSize), MessageType.Warning, true);

			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("TextureWidth"));
			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("TextureHeight"));

			SerializedProperty useMainCameraProperty = settingsProperty.FindPropertyRelative("UseMainCamera");

			_ = EditorGUILayout.PropertyField(useMainCameraProperty);

			if (!useMainCameraProperty.boolValue)
			{
				_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("TextureBackgroundColor"));

				SerializedProperty useOrthographicCameraProperty = settingsProperty.FindPropertyRelative("UseOrthographicCamera");

				_ = EditorGUILayout.PropertyField(useOrthographicCameraProperty);

				if (useOrthographicCameraProperty.boolValue)
				{
					_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("CameraSizeOrFieldOfView"), new GUIContent("Orthographic Size"));
				}
				else
				{
					_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("CameraSizeOrFieldOfView"), new GUIContent("Field Of View"));
				}

				_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("CameraClearFlags"));

				_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("CameraOffset"));
			}
			else
			{
				EditorGUILayout.HelpBox("Some options are disabled when 'Use Main Camera' is enabled.", MessageType.Info, true);
			}

			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("RotateOnXAxis"));
			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("RotateOnYAxis"));
			_ = EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("RotateOnZAxis"));

			_ = serializedObject.ApplyModifiedProperties();
		}
	}
}