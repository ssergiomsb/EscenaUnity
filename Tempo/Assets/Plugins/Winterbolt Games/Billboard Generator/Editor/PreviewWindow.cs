﻿using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

public class PreviewWindow : EditorWindow
{
	public readonly List<Texture2D> previewTextures = new List<Texture2D>();

	private Texture2D _previewTexture;

	private int _previewIndex;

	private float _previewSpeed = 0.5f;

	private float _counter;

	private void OnGUI()
	{
		if (previewTextures == null || previewTextures.Count == 0)
		{
			GUILayout.Label("No Textures");

			return;
		}

		_previewSpeed = EditorGUILayout.Slider("Preview Speed", _previewSpeed, 0.0125f, 1.25f);

		if (_counter >= _previewSpeed)
		{
			_previewIndex++;

			if (_previewIndex > previewTextures.Count - 1)
			{
				_previewIndex = 0;
			}

			_previewTexture = previewTextures[_previewIndex];

			_counter = 0.0f;
		}
		else
		{
			_counter += 0.1f;
		}

		Rect textureRect = new Rect(8.0f, GUILayoutUtility.GetLastRect().yMax + 8.0f, position.width - 16.0f, position.height - 32.0f);

		GUI.Box(textureRect, string.Empty);

		if (_previewTexture != null)
		{
			GUI.DrawTexture(textureRect, _previewTexture);
		}

		Repaint();
	}
}