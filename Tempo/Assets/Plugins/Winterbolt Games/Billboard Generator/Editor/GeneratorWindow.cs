﻿using System;
using System.Collections.Generic;
using System.IO;

using UnityEditor;

using UnityEngine;

using WinterboltGames.Tools.BillboardGenerator.Runtime;
using WinterboltGames.Utilities;

using Object = UnityEngine.Object;

namespace WinterboltGames.Tools.BillboardGenerator.Editor
{
	public class GeneratorWindow : EditorWindow
	{
		private class Entry
		{
			public bool IsExpanded;

			public GameObject GameObject;

			public GeneratorSettingsAsset SettingsAsset;

			public byte AutoCameraOffsetMode;
		}

		private readonly List<Entry> _entries = new List<Entry>();

		private Vector2 _scrollView;

		private static Color GetMoccasinColor(float alpha = 1.0f)
		{
			if (ColorUtility.TryParseHtmlString("#FFE4B5", out Color color))
			{
				color.a = alpha;

				return color;
			}

			return Color.clear;
		}

		private static Color GetFirebrickColor(float alpha = 1.0f)
		{
			if (ColorUtility.TryParseHtmlString("#B22222", out Color color))
			{
				color.a = alpha;

				return color;
			}

			return Color.clear;
		}

		[MenuItem("Tools/Winterbolt Games/Billboard Generator")] private static void CreateWindow() => GetWindow<GeneratorWindow>().titleContent = new GUIContent("Billboard Generator");

		private void OnGUI()
		{
			GUILayoutDropBox("(Drag And Drop GameObjects Here)", 32.0f, (Action<GameObject[]>)(gameObjects =>
			{
				foreach (GameObject gameObject in gameObjects)
				{
					_entries.Add(new Entry() { GameObject = gameObject, });
				}
			}));

			_scrollView = EditorGUILayout.BeginScrollView(_scrollView);

			for (int i = 0; i < _entries.Count; i++)
			{
				Entry entry = _entries[i];

				if (entry == null)
				{
					continue;
				}

				GUILayout.BeginHorizontal();

				entry.IsExpanded = GUILayout.Toggle(entry.IsExpanded, entry.GameObject == null ? "Missing" : entry.GameObject.name, EditorStyles.toolbarButton);

				entry.GameObject = DropBoxHandler(entry.GameObject, GUILayoutUtility.GetLastRect());

				GUI.color = GetFirebrickColor(0.75f);

				if (GUILayout.Button("Remove", EditorStyles.toolbarButton, GUILayout.MaxWidth(64.0f)))
				{
					_entries.RemoveAt(i);
				}

				GUI.color = Color.white;

				GUILayout.EndHorizontal();

				if (entry.IsExpanded)
				{
					entry.SettingsAsset = EditorGUILayout.ObjectField("Settings Asset", entry.SettingsAsset, typeof(GeneratorSettingsAsset), false) as GeneratorSettingsAsset;

					if (entry.SettingsAsset != null)
					{
						if (!entry.SettingsAsset.Settings.UseMainCamera && GUILayout.Button($"Auto Camera Offset Mode: {(entry.AutoCameraOffsetMode == 0 ? "Mesh" : "Collider")}", EditorStyles.toolbarButton))
						{
							entry.AutoCameraOffsetMode = (byte)(entry.AutoCameraOffsetMode == 0 ? 1 : 0);
						}

						if (!entry.SettingsAsset.Settings.UseMainCamera && GUILayout.Button("Calculate Auto Camera Offset", EditorStyles.toolbarButton))
						{
							if (entry.AutoCameraOffsetMode == 0)
							{
								MeshFilter meshFilter = entry.GameObject.GetComponent<MeshFilter>();

								if (meshFilter == null)
								{
									MeshRenderer meshRenderer = entry.GameObject.GetComponent<MeshRenderer>();

									if (meshRenderer == null)
									{
										Debug.LogError($"{entry.GameObject.name} doesn't have a MeshFilter or a MeshRenderer attached to it.", entry.GameObject);
									}
									else
									{
										Vector3 offset = meshRenderer.bounds.center;

										offset.z = -Vector3.Distance(meshRenderer.bounds.center, meshRenderer.bounds.extents);

										entry.SettingsAsset.Settings.CameraOffset = offset;
									}
								}
								else
								{
									Vector3 offset = meshFilter.mesh.bounds.center;

									offset.z = -Vector3.Distance(meshFilter.mesh.bounds.center, meshFilter.mesh.bounds.extents);

									entry.SettingsAsset.Settings.CameraOffset = offset;
								}
							}
							else
							{
								Collider collider = entry.GameObject.GetComponent<Collider>();

								if (collider == null)
								{
									Debug.LogError($"{entry.GameObject.name} doesn't have a Collider attached to it.", entry.GameObject);
								}
								else
								{
									Vector3 offset = collider.bounds.center;

									offset.z = -Vector3.Distance(collider.bounds.center, collider.bounds.extents);

									entry.SettingsAsset.Settings.CameraOffset = offset;
								}
							}
						}

						if (GUILayout.Button("Generate", EditorStyles.toolbarButton))
						{
							List<Texture2D> textures = new List<Texture2D>();

							Generator.Generate(entry.GameObject, entry.SettingsAsset.Settings, progress: textures.Add);

							GetWindow<PreviewWindow>().previewTextures.AddRange(textures);

							string outputFolderPath = EditorUtility.OpenFolderPanel("Choose Output Folder...", Application.dataPath, string.Empty);

							if (outputFolderPath.Length != 0)
							{
								foreach (Texture2D texture in textures)
								{
									File.WriteAllBytes(Path.Combine(outputFolderPath, $"{texture.name}.png"), texture.EncodeToPNG());
								}
							}

							AssetDatabase.Refresh();
						}

						if (GUILayout.Button("Generate Atlas", EditorStyles.toolbarButton))
						{
							List<Texture2D> textures = new List<Texture2D>();

							Generator.Generate(entry.GameObject, entry.SettingsAsset.Settings, progress: textures.Add);

							int textureWidth = entry.SettingsAsset.Settings.TextureWidth;
							int textureHeight = entry.SettingsAsset.Settings.TextureHeight;

							int columns = textures.Count / 8;

							int rows = Mathf.CeilToInt((float)textures.Count / columns);

							int atlasWidth = columns * textureWidth;
							int atlasHeight = rows * textureHeight;

							Texture2D atlas = new Texture2D(atlasWidth, atlasHeight, TextureFormat.ARGB32, false, false);

							List<UVCoordinates> uvCoordinates = new List<UVCoordinates>();

							int index = 0;

							for (int y = 0; y < rows; y++)
							{
								for (int x = 0; x < columns; x++)
								{
									atlas.SetPixels32(x * textureWidth, y * textureHeight, textureWidth, textureHeight, textures[index].GetPixels32());

									uvCoordinates.Add(new UVCoordinates(x * textureWidth, y * textureHeight, textureWidth, textureHeight, atlasWidth, atlasHeight));

									index++;
								}
							}

							atlas.Apply();

							string outputFolderPath = EditorUtility.OpenFolderPanel("Choose Output Folder...", Application.dataPath, string.Empty);

							if (outputFolderPath.Length != 0)
							{
								string atlasTexturePath = Path.Combine(outputFolderPath, $"{entry.GameObject.name} Billboard Atlas Texture.png");

								File.WriteAllBytes(atlasTexturePath, atlas.EncodeToPNG());

								AssetDatabase.Refresh();

								BillboardAtlasAsset atlasAsset = CreateInstance<BillboardAtlasAsset>();

								atlasAsset.name = $"{entry.GameObject.name} Billboard Atlas";

								atlasAsset.RendererScale = CalculateTransformVolume(entry.GameObject.transform);

								atlasAsset.UVCoordinates = uvCoordinates;

								AssetDatabase.CreateAsset(atlasAsset, $"Assets{outputFolderPath.Substring(Application.dataPath.Length)}/{entry.GameObject.name} Billboard Atlas.asset");
							}
						}
					}
				}
			}

			EditorGUILayout.EndScrollView();
		}

		private T DropBoxHandler<T>(T value, Rect rect, Action<T> onDrop = null) where T : Object
		{
			Event currentEvent = Event.current;

			if (!(currentEvent.type == EventType.DragUpdated || currentEvent.type == EventType.DragPerform))
			{
				return value;
			}

			if (!rect.Contains(currentEvent.mousePosition))
			{
				return value;
			}

			Object objectReference = DragAndDrop.objectReferences[0];

			if (objectReference is T)
			{
				DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
			}
			else
			{
				DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;

				return value;
			}

			if (currentEvent.type != EventType.DragPerform)
			{
				return value;
			}

			DragAndDrop.AcceptDrag();

			currentEvent.Use();

			if (onDrop != null)
			{
				onDrop.Invoke(objectReference as T);
			}

			return objectReference as T;
		}

		private T[] DropBoxHandler<T>(Rect rect, Action<T[]> onDrop = null) where T : Object
		{
			Event currentEvent = Event.current;

			if (!(currentEvent.type == EventType.DragUpdated || currentEvent.type == EventType.DragPerform))
			{
				return new T[0];
			}

			if (!rect.Contains(currentEvent.mousePosition))
			{
				return new T[0];
			}

			List<T> objectReferences = new List<T>();

			foreach (Object objectReference in DragAndDrop.objectReferences)
			{
				T tObjectReference = objectReference as T;

				if (tObjectReference != null)
				{
					objectReferences.Add(tObjectReference);
				}
			}

			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

			if (currentEvent.type != EventType.DragPerform)
			{
				return new T[0];
			}

			DragAndDrop.AcceptDrag();

			currentEvent.Use();

			if (onDrop != null)
			{
				onDrop.Invoke(objectReferences.ToArray());
			}

			return objectReferences.ToArray();
		}

		private void GUILayoutDropBox<T>(string label, float height = 24.0f, Action<T[]> onDrop = null) where T : Object
		{
			GUI.color = GetMoccasinColor(0.625f);

			GUILayout.Box(label, EditorStyles.centeredGreyMiniLabel, GUILayout.Height(height));

			GUI.color = Color.white;

			DropBoxHandler(GUILayoutUtility.GetLastRect(), onDrop);
		}

		private Vector3 CalculateTransformVolume(Transform transform)
		{
			if (transform == null)
			{
				return new Vector3(float.NaN, float.NaN, float.NaN);
			}

			Bounds volume = new Bounds();

			List<Transform> children = TransformUtilities.GetChildrenRecursive(transform);

			foreach (Transform child in children)
			{
				Renderer renderer = child.GetComponent<Renderer>();

				if (renderer != null)
				{
					volume.Encapsulate(renderer.bounds);
				}
			}

			return volume.size;
		}
	}
}