﻿using UnityEditor;

using WinterboltGames.Tools.BillboardGenerator.Runtime;

namespace WinterboltGames.Tools.BillboardGenerator.Editor
{
	[CustomEditor(typeof(RuntimeBillboardRenderer)), CanEditMultipleObjects]
	public class RuntimeBillboardRendererInspector : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			SerializedObject serializedObject = new SerializedObject(target);

			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_target"));

			SerializedProperty useAlternativeWorkflowProperty = serializedObject.FindProperty("_useAlternativeWorkflow");

			_ = EditorGUILayout.PropertyField(useAlternativeWorkflowProperty);

			if (useAlternativeWorkflowProperty.boolValue)
			{
				_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_meshFilter"));

				_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_meshRenderer"));
			}
			else
			{
				_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_material"));
			}

			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_atlas"));

			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_update"));
			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("_render"));

			_ = serializedObject.ApplyModifiedProperties();
		}
	}
}