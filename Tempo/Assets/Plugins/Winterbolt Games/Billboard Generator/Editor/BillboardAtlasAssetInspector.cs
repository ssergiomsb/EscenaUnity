﻿using UnityEditor;

using WinterboltGames.Tools.BillboardGenerator.Runtime;

namespace WinterboltGames.Tools.BillboardGenerator.Editor
{
	[CustomEditor(typeof(BillboardAtlasAsset)), CanEditMultipleObjects]
	public class BillboardAtlasAssetInspector : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			SerializedObject serializedObject = new SerializedObject(target);

			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("RendererScale"));

			EditorGUI.BeginDisabledGroup(true);

			_ = EditorGUILayout.PropertyField(serializedObject.FindProperty("UVCoordinates"), true);

			EditorGUI.EndDisabledGroup();

			_ = serializedObject.ApplyModifiedProperties();
		}
	}
}